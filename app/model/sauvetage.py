from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime
from app import db


class Sauvetage(db.Model):
    __tablename__ = "sauvetage"
    sauve_id = Column(Integer(), primary_key=True)
    sauve_creation = Column(DateTime(), primary_key=True, nullable=False)
    coord_gps = Column(String(255))
    date_sauve = Column(DateTime(), nullable=False)
    desc = Column(String(255))
    valide_sauve = Column(Boolean(), nullable=False)

    bat1_id = Column('bat_sauveteur_id', Integer(), ForeignKey('bateau.bat_id'))
    bat2_id = Column('bat_secourus_id', Integer(), ForeignKey('bateau.bat_id'))

