from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy import func
from app import db


class Personne(db.Model):
    __tablename__ = "personne"
    pers_id = Column(Integer(), primary_key=True)
    pers_creation = Column(DateTime(), primary_key=True, nullable=False)
    nom = Column(String(255))
    prenom = Column(String(255))
    date_naissance = Column(DateTime())
    date_deces = Column(DateTime())
    sauvetage = Column(Boolean(), nullable=False)
    valide_pers = Column(Boolean(), nullable=False)

    def __repr__(self):
        return self.nom + " " + self.prenom


def getPersonne(id):
    return Personne.query.filter(Personne.pers_id == id).all()[0]


def max_id_personne():
    max_logins = db.session.query(func.max(Personne.pers_id)).scalar()
    return max_logins if max_logins else 0


def all_personne():
    return Personne.query.all()


def AllPersonnePasValider():
    return  Personne.query.filter(Personne.valide_pers==False).all()