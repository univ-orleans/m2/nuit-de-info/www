from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime
from sqlalchemy.orm import relationship, backref

from app import db


class Status(db.Model):
    __tablename__ = "status"
    status_id = Column(Integer(), primary_key=True)
    intitule = Column(String(255))

    def __repr__(self):
        return self.intitule


class Compose(db.Model):
    __tablename__ = "compose"
    compose_id = Column(Integer(), primary_key=True)
    valide_status = Column(Boolean(), primary_key=True, nullable=False)
    pers_id = Column('pers_id', Integer(), ForeignKey('personne.pers_id'))
    bat_id = Column('bat_id', Integer(), ForeignKey('bateau.bat_id'))
    status_id = Column('status_id', Integer(), ForeignKey('status.status_id'))


def getAllStatus():
    return Status.query.all()


def getComposeBateau(id):
    """
    retourne la liste de la composition d'un bateau
    """
    return Compose.query.filter(Compose.bat_id == id).all()
