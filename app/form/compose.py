# IMPORTS #####################################################################
from wtforms import StringField, BooleanField, SubmitField, SelectField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from app.model.personne import all_personne
from app.model.compose import getAllStatus


# FORMS #######################################################################
class StatusForm(FlaskForm):
    intutile = StringField(label="Intitulé", validators=[DataRequired()])
    submit = SubmitField("Ajouter")


class ComposeForm(FlaskForm):
    pers_id = SelectField(label="Personne à ajouter : ")
    # bat_id = SelectField(label="Bateau de sauvetage ?")
    status_id = SelectField(label="Status : ")
    submit = SubmitField("Ajouter")
